package demo.steps.serenity;

import demo.pages.DictionaryPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserSteps {

    DictionaryPage dictionaryPage;

    @Step
    public void pressSubscribeButton() {
        dictionaryPage.pressSubscribeButton();
    }

    @Step
    public void pressContactButton() {
        dictionaryPage.pressContactButton();
    }

    @Step
    public void enterEmail(String keyword) {
        dictionaryPage.enterEmail(keyword);
    }

    @Step
    public void enterParola(String keyword) {
        dictionaryPage.enterPassword(keyword);
    }

    @Step
    public void pressNotLoggedButton() {
        dictionaryPage.pressNotLoggedButton();
    }

    @Step
    public void pressLoginButton() {
        dictionaryPage.pressLoginButton();
    }

    @Step
    public void pressLogOutButton()
    {
        dictionaryPage.pressLogOutButton();
    }

    @Step
    public void should_see_definition(String definition) {
        assertThat(dictionaryPage.getDefinitions(), hasItem(containsString(definition)));
    }

    @Step
    public void shouldSeeFailedLogInMessage(String text) {
        try {
            assertThat(dictionaryPage.getFailedLogInMessage(), containsString(text));
        } catch (Exception ex) {
        }
    }

    @Step
    public void shouldSeeValidLogInEmail(String text) {
        try {
            assertThat(dictionaryPage.getValidLogInEmail(), containsString(text));
        } catch (Exception ex) {
        }
    }

    @Step
    public void shouldSeeValidContact(String text) {
        try {
            assertThat(dictionaryPage.getValidContact(), containsString(text));
        } catch (Exception ex) {
        }
    }

    @Step
    public void shouldSeeLogOutContact(String text) {
        try {
            assertThat(dictionaryPage.getValidLogOut(), containsString(text));
        } catch (Exception ex) {
        }
    }

    @Step
    public void shouldSeeValidSubscribe(String text) {
        try {
            assertThat(dictionaryPage.getValidSubscribe(), containsString(text));
        } catch (Exception ex) {
        }
    }


    @Step
    public void is_the_home_page() {
        dictionaryPage.open();
    }
}