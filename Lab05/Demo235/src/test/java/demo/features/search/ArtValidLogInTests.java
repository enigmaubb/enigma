package demo.features.search;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import demo.steps.serenity.EndUserSteps;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom("src/test/resources/TestValidData.csv")
public class ArtValidLogInTests {

    @Managed(uniqueSession = true, driver="firefox")
    public WebDriver webdriver;

    @Steps
    public EndUserSteps step;

    String email, password, name;


   @Issue("#LogIn")
    @Test
    public void ValidLogin() {
        step.is_the_home_page();
        step.pressNotLoggedButton();
        step.enterEmail(email);
        step.enterParola(password);
        step.pressLoginButton();
        step.shouldSeeValidLogInEmail("Nume: "+name);
    }
}