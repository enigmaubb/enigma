package demo.features.search;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import demo.steps.serenity.EndUserSteps;

@RunWith(SerenityRunner.class)
public class ArtValidScenarioTests {

    @Managed(uniqueSession = true, driver = "firefox")
    public WebDriver webdriver;

    @Steps
    public EndUserSteps step;

    @Issue("#LogIn")
    @Test
    public void ValidLogin() {
        step.is_the_home_page();
        step.pressNotLoggedButton();
        step.enterEmail("franciuc.alexandru10@gmail.com");
        step.enterParola("ana123456");
        step.pressLoginButton();
        step.shouldSeeValidLogInEmail("Nume: Franciuc");

        step.pressContactButton();
        step.shouldSeeValidContact("Editura ART");

        step.pressSubscribeButton();
        step.shouldSeeValidSubscribe("Introduceţi adresa dumneavoastră de email");

        step.is_the_home_page();
        step.pressLogOutButton();
        step.shouldSeeLogOutContact("Intră în cont");
    }
}