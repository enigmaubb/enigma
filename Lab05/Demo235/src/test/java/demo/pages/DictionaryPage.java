package demo.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.util.List;

@DefaultUrl("https://www.editura-art.ro/")
public class DictionaryPage extends PageObject {

    @FindBy(id = "not_logged_icon")
    private WebElementFacade notLoggedButton;

    @FindBy(id = "username_login")
    private WebElementFacade email;

    @FindBy(id = "password_login")
    private WebElementFacade parola;

    @FindBy(id = "forml")
    private WebElementFacade loginButton;


    public void pressSubscribeButton() {
        find(By.xpath("//a[@href='https://www.editura-art.ro/protectia-datelor/subscribe']")).click();
    }

    public void pressContactButton() {
        find(By.xpath("//a[@href='https://www.editura-art.ro/editura']")).click();
    }

    public void pressLogOutButton() {
        find(By.xpath("//a[@href='/user/logout']")).click();
    }

    public void pressNotLoggedButton() {
        notLoggedButton.click();
    }

    public void pressLoginButton() {
        loginButton.submit();
    }

    public void enterEmail(String keyword) {
        email.type(keyword);
    }

    public void enterPassword(String keyword) {
        parola.type(keyword);
    }

    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("ol"));
        return definitionList.findElements(By.tagName("li")).stream()
                .map(element -> element.getText())
                .collect(Collectors.toList());
    }

    public String getFailedLogInMessage() throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElementFacade definitionList = find(By.className("error_message"));
        String mere = definitionList.getText();
        return mere;
    }

    public String getValidLogInEmail() throws Exception {
        TimeUnit.SECONDS.sleep(2);
        WebElementFacade definitionList = find(By.className("user_data"));
        String mere = definitionList.getText();
        return mere;
    }

    public String getValidContact() throws Exception {
        TimeUnit.SECONDS.sleep(2);
        WebElementFacade definitionList = find(By.className("main_title"));
        String mere = definitionList.getText();
        return mere;
    }

    public String getValidSubscribe() throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElementFacade definitionList = find(By.className("gdpr-central"));
        String mere = definitionList.getText();
        return mere;
    }
    public String getValidLogOut() throws Exception {
        TimeUnit.SECONDS.sleep(1);
        WebElementFacade definitionList = find(By.className("form_left"));
        String mere = definitionList.getText();
        return mere;
    }
}