package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class InventoryServiceTest {
    private String name = "Part1";
    private double price = 6.4;
    private int inStock = 3;
    private int min = 1;
    private int max = 8;
    private int partId = 999;
    private int machineId = 3;

    private InventoryRepository inventoryRepository;
    private InventoryService service;
    private int numberOfParts;
    private InhousePart part;

    @BeforeEach
    void setUp() {
        inventoryRepository = new InventoryRepository();
        service = new InventoryService(inventoryRepository);
        numberOfParts = inventoryRepository.getAllParts().size();
        part = new InhousePart(partId, name, price, inStock, min, max, machineId);
    }

    @Test
    void addPart_ECP_Valid() {
        try {
            service.addInhousePart(part);
        } catch (Exception ex) {
        }

        assertEquals(numberOfParts + 1, inventoryRepository.getAllParts().size());
        assertEquals(service.lookupPart(name).getName(), name);
        assertEquals(service.lookupPart(name).getPrice(), price);
        assertEquals(service.lookupPart(name).getInStock(), inStock);
    }

    @Test
    void addPart_ECP_NonValid_String_Empty() {
        try {
            part.setName("");
            service.addInhousePart(part);
        } catch (Exception ex) {
        }

        assertEquals(numberOfParts, inventoryRepository.getAllParts().size());
        assertNull(service.lookupPart(""));
    }

    @AfterEach
    void tearDown() {
        service.deletePart(part);
    }
}