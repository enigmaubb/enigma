package inventory.service.mock;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ServiceTestWithMockConstructorTest {

    private String name1 = "Part77";
    private String name2 = "Part88";
    private double price = 6.4;
    private int inStock = 3;
    private int min = 1;
    private int max = 8;
    private int partId = 999;
    private int machineId = 3;
    private InhousePart part;
    private InhousePart part1;

    @Mock
    private InventoryRepository repo;

    @InjectMocks
    private InventoryService service;

    @BeforeEach
    public void setUp() {
        repo = mock(InventoryRepository.class);
        service = new InventoryService(repo);
    }

    @Test
    public void add_invaliddPart()throws Exception {
        InhousePart p1 = new InhousePart(partId, name1, price, inStock, min, max, machineId);
        InhousePart p2 = new InhousePart(partId, "", price, inStock, min, max, machineId);

        Mockito.doThrow(Exception.class).when(repo).addPart(p2);

        service.addInhousePart(p1);

        Mockito.verify(repo, times(1)).addPart(p1);
    }

    @Test
    public void add_validdPart()  throws Exception{
        InhousePart p1 = new InhousePart(partId, name1, price, inStock, min, max, machineId);
        InhousePart p2 = new InhousePart(partId, name2, price, inStock, min, max, machineId);
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableArrayList(p2));
        Mockito.doNothing().when(repo).addPart(p1);

        service.addInhousePart(p1);

        Mockito.verify(repo, times(1)).addPart(p1);
        Mockito.verify(repo, never()).getAllParts();

        assert true;
        assertEquals(1, repo.getAllParts().size());
        assert 1 == service.getAllParts().size();

        Mockito.verify(repo, times(2)).getAllParts();
    }

    @AfterEach
    public void tearDown() {
        service = null;
    }
}
