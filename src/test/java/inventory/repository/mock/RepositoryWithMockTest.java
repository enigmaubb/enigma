package inventory.repository.mock;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.validator.Validator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

public class RepositoryWithMockTest {
    @InjectMocks
    private InventoryRepository repo;

    @Mock
    private Validator validator;

    private String name = "Part777";
    private double price = 6.4;
    private int inStock = 3;
    private int min = 1;
    private int max = 8;
    private int partId = 999;
    private int machineId = 3;
    private InhousePart part;
    private InhousePart part1;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        part = mock(InhousePart.class);
        part1 = mock(InhousePart.class);
    }

    @Test
    public void add_invalidPart() {
        assert 10 == repo.getAllParts().size();
        System.out.println(repo.getAllParts().toString());
        Mockito.when(validator.isValidPartt(part)).thenReturn(new ArrayList<String>(Collections.singleton("a")));

        try {
            repo.addPart(part);
        } catch (Exception e) {
            e.printStackTrace();
            assert 10 == repo.getAllParts().size();
            System.out.println(repo.getAllParts().toString());
        }

        Mockito.verify(validator, times(1)).isValidPartt(part);
    }

    @Test
    public void add_validPart() {
        assert 10 == repo.getAllParts().size();
        System.out.println(repo.getAllParts().toString());
        Mockito.when(validator.isValidPartt(part)).thenReturn(new ArrayList<String>(Collections.singleton("a")));
        part1 = new InhousePart(partId, name, price, inStock, min, max, machineId);
        try {
            repo.addPart(part1);
        } catch (Exception e) {
            e.printStackTrace();
            assert 10 == repo.getAllParts().size();
            System.out.println(repo.getAllParts().toString());
        }

        assert 11 == repo.getAllParts().size();
        System.out.println(repo.getAllParts().toString());

        Mockito.verify(validator, times(1)).isValidPartt(part1);
    }

    @AfterEach
    public void tearDown() {
        repo.deletePart(part1);
        repo = null;
        part = null;
    }
}
