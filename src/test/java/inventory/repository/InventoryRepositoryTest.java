package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.Inventory;
import inventory.model.Part;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@Nested
class InventoryRepositoryTest {

    private Part part;
    private String name = "Part1";
    private double price = 6.4;
    private int inStock = 3;
    private int min = 1;
    private int max = 8;
    private int partId = 4;
    private int machineId = 3;

    private InventoryRepository inventoryRepository;
    private int numberOfParts;

    @BeforeEach
    void setUp() {
        part = new InhousePart(partId, name, price, inStock, min, max, machineId);
        inventoryRepository = new InventoryRepository();
        numberOfParts = inventoryRepository.getAllParts().size();
    }

    @AfterEach
    void tearDown() {
        inventoryRepository.deletePart(part);
    }

    @DisplayName("AddPart ECP Valid")
    @Test
    @Tag("ECP")
    @Order(1)
    void addPart_ECP_Valid() {
        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        assertEquals(numberOfParts + 1, inventoryRepository.getAllParts().size());
    }

    @Test
    @DisplayName("AddPart ECP NonValid String Empty")
    @Tag("ECP")
    @Order(2)
    void addPart_ECP_NonValid_String_Empty() {
        //Arrange
        part.setName("");

        //Act
        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        //Assert
        assertEquals(numberOfParts, inventoryRepository.getAllParts().size());
    }

    @Test
    @DisplayName("AddPart ECP NonValid inStock")
    @Tag("ECP")
    @Order(3)
    void addPart_ECP_NonValid_inStock() {
        //Arrange
        part.setInStock(-100);

        //Act
        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        //Assert
        assertEquals(numberOfParts, inventoryRepository.getAllParts().size());
    }

    @Test
    @Order(4)
    @Tag("BVA")
    void addPart_BVA_Valid_inStock() {
        //Arrange
        part.setInStock(1);

        //Act
        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        //Assert
        assertEquals(numberOfParts + 1, inventoryRepository.getAllParts().size());
    }

    @Test
    @Order(5)
    @Tag("BVA")
    void addPart_BVA_NonValid_Length_Is_256() {
        //Arrange
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < 256; i++) {
            name.append("p");
        }
        part.setName(name.toString());

        //Act
        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        //Assert
        assertEquals(numberOfParts+1, inventoryRepository.getAllParts().size());
    }

    @Test
    @Order(6)
    @Tag("BVA")
    void addPart_BVA_Valid_Length_Is_255() {
        //Arrange
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < 255; i++) {
            name.append("p");
        }
        part.setName(name.toString());

        //Act
        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        //Assert
        assertEquals(numberOfParts + 1, inventoryRepository.getAllParts().size());
    }

    @Test
    @Order(7)
    @Tag("BVA")
    void addPart_BVA_NonValid_inStock_Invalid() {
        part.setInStock(0);

        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        assertEquals(numberOfParts, inventoryRepository.getAllParts().size());
    }

    @Test
    @Order(8)
    @Tag("BVA")
    void addPart_BVA_NonValid_inStock_Too_Big() {
        part.setInStock(Integer.MAX_VALUE + 1);

        try {
            inventoryRepository.addPart(part);
        } catch (Exception ex) {
        }

        assertEquals(numberOfParts, inventoryRepository.getAllParts().size());
    }
}
