package inventory.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PartTest {
    private String name = "Part1";
    private double price = 6.4;
    private int inStock = 3;
    private int min = 1;
    private int max = 8;
    private int partId = 4;
    private int machineId = 3;

    @Test
    void Get_CreatePart() {
        Part newPart = new InhousePart(partId, name, price, inStock, min, max, machineId);

        assertEquals(newPart.getName(), name);
        assertEquals(newPart.getPrice(), price);
        assertEquals(newPart.getInStock(), inStock);
        assertEquals(newPart.getMin(), min);
        assertEquals(newPart.getMax(), max);
    }

    @Test
    void Set_CreatePart() {
        String newName = "Test";
        double newPrice = 44;
        int newInStock = 4;
        Part newPart = new InhousePart(partId, name, price, inStock, min, max, machineId);

        newPart.setName(newName);
        newPart.setPrice(newPrice);
        newPart.setInStock(newInStock);

        assertEquals(newPart.getName(), newName);
        assertEquals(newPart.getPrice(), newPrice);
        assertEquals(newPart.getInStock(), newInStock);
    }
}