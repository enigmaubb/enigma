package inventory.model;

import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {

    private Inventory inventory;
    private final Product product1 = new Product(1, "P1", 1, 2, 1, 9, null);
    private final Product product2 = new Product(1, "P2", 1, 2, 1, 9, null);
    private final Product product3 = new Product(1, "P3", 1, 2, 1, 9, null);
    private final Product product4 = new Product(1, "P4", 1, 2, 1, 9, null);

    @BeforeEach
    void setUp() {
        inventory = new Inventory();
    }

    @Test
    void ProductsEmpty() {

        Product product = inventory.lookupProduct("P1");

        assertNull(product);
    }

    @Test
    void SearchItemFound() {
        inventory.addProduct(product1);
        inventory.addProduct(product2);

        Product product = inventory.lookupProduct("P2");

        assertEquals(product, product2);
    }

    @Test
    void SearchItemFound1() {
        inventory.addProduct(product1);
        inventory.addProduct(product2);
        inventory.addProduct(product3);
        inventory.addProduct(product4);

        Product product = inventory.lookupProduct("P3");

        assertEquals(product, product3);
    }

    @Test
    void SearchItemNotFound() {
        inventory.addProduct(product1);
        inventory.addProduct(product3);

        Product product = inventory.lookupProduct("P2");

        assertNull(product);
    }

    @AfterEach
    void tearDown() {
        inventory.removeProduct(product1);
        inventory.removeProduct(product2);
        inventory.removeProduct(product3);
        inventory.removeProduct(product4);
    }

}