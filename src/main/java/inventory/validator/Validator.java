package inventory.validator;

import inventory.model.Part;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.List;

public class Validator {
    /**
     * Generate an error message for invalid values in a product
     * and evaluate whether the sum of the price of associated parts
     * is less than the price of the resulting product.
     * A valid product will return an empty error message string.
     *
     * @param name
     * @param min
     * @param max
     * @param inStock
     * @param price
     * @param parts
     * @param errorMessage
     * @return
     */
    public static String isValidProduct(String name, double price, int inStock, int min, int max, ObservableList<Part> parts, String errorMessage) {
        double sumOfParts = 0.00;
        for (int i = 0; i < parts.size(); i++) {
            sumOfParts += parts.get(i).getPrice();
        }
        if (name.equals("")) {
            errorMessage += "A name has not been entered. ";
        }
        if (min < 0) {
            errorMessage += "The inventory level must be greater than 0. ";
        }
        if (price < 0.01) {
            errorMessage += "The price must be greater than $0. ";
        }
        if (min > max) {
            errorMessage += "The Min value must be less than the Max value. ";
        }
        if (inStock < min) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if (inStock > max) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        if (parts.size() < 1) {
            errorMessage += "Product must contain at least 1 part. ";
        }
        if (sumOfParts > price) {
            errorMessage += "Product price must be greater than cost of parts. ";
        }
        return errorMessage;
    }

    /**
     * Generate an error message for invalid values in a part
     * Valid part will return an empty string
     *
     * @param name
     * @param price
     * @param inStock
     * @param min
     * @param max
     * @param errorMessage
     * @return
     */
    public static String isValidPart(String name, double price, int inStock, int min, int max, String errorMessage) {
        if (name == "") {
            errorMessage += "A name has not been entered.";
        }
        if (price < 0.01) {
            errorMessage += "The price must be greater than 0.";
        }
        if (inStock < 1) {
            errorMessage += "Inventory level must be greater than 0.";
        }
        if (min > max) {
            errorMessage += "The Min value must be less than the Max value.";
        }
        if (inStock < min) {
            errorMessage += "Inventory level is lower than minimum value. ";
        }
        if (inStock > max) {
            errorMessage += "Inventory level is higher than the maximum value. ";
        }
        return errorMessage;
    }

    public List<String> isValidPartt(Part part) {
        List<String> errors = new ArrayList<String>();
        if (part.getName() == "") {
            errors.add("A name has not been entered.");
        }
        //if (part.getName().length() < 1 || part.getName().length() > 255) {
        //    errors.add("Name  length is invalid, should be in [1, 255]");
       // }
        if (part.getPrice() < 0.01) {
            errors.add("The price must be greater than 0.");
        }
        if (part.getInStock() < 1) {
            errors.add("Inventory level must be greater than 0.");
        }
         if (part.getMin() > part.getMax()) {
            errors.add("The Min value must be less than the Max value.");
        }
        if (part.getInStock() < part.getMin()) {
            errors.add("Inventory level is lower than minimum value.");
        }
        if (part.getInStock() > part.getMax()) {
            errors.add("Inventory level is higher than the maximum value.");
        }
        return errors;
    }
}
